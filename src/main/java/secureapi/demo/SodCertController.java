package secureapi.demo;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.List;
import org.jmrtd.lds.SODFile;
// import org.ssha.sjas;

class SodRequest {
    private String sod;

    // Getters and setters (you can generate them in your IDE or write them manually)
    public String getSod() {
        return sod;
    }

    public void setSod(String sod) {
        this.sod = sod;
    }
}

@RestController
public class SodCertController {

	@PostMapping("/sod2cert")
	public String generateCertificate(@RequestBody SodRequest sodRequest) throws CertificateEncodingException {
        String cert_base64 = null;
        String sod = null;
        try {
            sod = sodRequest.getSod();
            String BEGIN_CERT = "-----BEGIN CERTIFICATE-----";
            String END_CERT = "-----END CERTIFICATE-----";
            String LINE_SEPARATOR = System.getProperty("line.separator");
            Base64.Encoder encoder = Base64.getMimeEncoder(64, LINE_SEPARATOR.getBytes());

            byte[] decodedBytes = decode(sod);
            System.out.println(new String(encoder.encode((decodedBytes))).replace(System.getProperty("line.separator"), ""));
            System.out.println("------");
            InputStream inputStream = new ByteArrayInputStream(decodedBytes);
            SODFile sodFile = new SODFile(inputStream);
            List<X509Certificate> documentSigner = sodFile.getDocSigningCertificates();

            // gen document signature certificate
            byte[] rawCrtText = documentSigner.get(0).getEncoded();
            String encodedCertText = new String(encoder.encode(rawCrtText));
            String prettified_cert = BEGIN_CERT + LINE_SEPARATOR + encodedCertText + LINE_SEPARATOR + END_CERT;
            System.out.println(encodedCertText.replace(System.getProperty("line.separator"), ""));
            System.out.println("------");
            System.out.println(prettified_cert);
            System.out.println("------");
            System.out.println(new String(encoder.encode(prettified_cert.getBytes())));
            cert_base64 = new String(encoder.encode(prettified_cert.getBytes()))
                    .replace(System.getProperty("line.separator"), "");
            return cert_base64;
        } catch (IOException e) {
            e.printStackTrace();
            // Handle the exception as needed
            return "Error generating certificate";
        }

    }

    private byte[] decode(String raw) {
        if (raw != null && !raw.trim().isEmpty()) {
            return Base64.getDecoder().decode(raw.replaceAll("\\s", ""));
        }
        return null;
    }
	

}





