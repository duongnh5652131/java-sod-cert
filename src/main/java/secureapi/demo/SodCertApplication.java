package secureapi.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SodCertApplication {

	public static void main(String[] args) {
		SpringApplication.run(SodCertApplication.class, args);
	}

}
