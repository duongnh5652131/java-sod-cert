# Use an official Maven image as the base image
# FROM maven:3.8.4-openjdk-11-slim AS build
# # Set the working directory in the container
# WORKDIR /app
# Copy the pom.xml and the project files to the container


# Build the application using Maven
# RUN mvn clean package -DskipTests
# Use an official OpenJDK image as the base image
FROM openjdk:17-jdk-slim
# Set the working directory in the container
WORKDIR /app
COPY pom.xml /app/.
ADD . /app/
# Copy the built JAR file from the previous stage to the container
# Set the command to run the application
# RUN chmod 777 start_server.sh
# CMD ./start_server.sh
# CMD ["java", "-jar", "demo.jar"]
CMD ["/usr/bin/env", "java", "-jar", "demo.jar"]
# CMD ["java", "-jar", "my-application.jar"]